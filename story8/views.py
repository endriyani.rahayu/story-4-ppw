from django.shortcuts import render, redirect
from django.http import HttpResponse
import json
from .models import *
import requests
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm



# Create your views here.
def fungsi_search_book(request):

    response ={}
    return render(request, 'story8.html', response)

def fungsi_perantara_book(request):
    if request.method == 'GET':
        if 'q' in request.GET:
            arg = request.GET['q']
        else:
            arg="a"
    else:
        arg = 'a'
    url_tujuan = f"https://www.googleapis.com/books/v1/volumes?q={arg}"
    r = requests.get(url_tujuan)
    data = r.json()
    return JsonResponse(data, safe=False)

def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            request.session['username'] = username
            messages.success(request, f"Your account has been created! You are now able to log in")
            return redirect('login')
    else:
        form = UserRegisterForm()
    response = {'form': form}
    return render(request, 'users/register.html', response)


@login_required
def profile(request):
    response = {}
    return render(request, 'users/profile.html', response)