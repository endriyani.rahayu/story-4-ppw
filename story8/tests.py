import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from story8.views import *
from django.contrib.auth.models import User

# Create your tests here.

class Story8Test(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('story8'))
        self.assertEqual(response.status_code, 200)

    def test_url_book(self):
        response = Client().get('/story8/')
        self.assertEqual(200,response.status_code)

    def test_template_book(self):
       response = Client().get('/story8/')
       self.assertTemplateUsed(response, 'story8.html')

    def test_func_page_book(self):
        found = resolve('/story8/') 
        self.assertEqual(found.func, fungsi_search_book)

    def test_view_profile(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/profile/')
        self.assertEqual(200,response.status_code)