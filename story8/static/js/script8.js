$('#kata_kunci').keyup(function(){
    var isi_keyword = $('#kata_kunci').val();
    // console.log(isi_keyword);

    var url_utk_dipanggil = /response/;
    // console.log(url_utk_dipanggil);
    
    $.ajax({
        method: 'GET',
        data:
        {'q':isi_keyword},
        url: url_utk_dipanggil,
        success: function(hasil){
            console.log(hasil.items);
            var obj_hasil = $('#tbody');
            obj_hasil.empty();

            for (i = 0; i < hasil.items.length; i++) {
                var tmp_title = hasil.items[i].volumeInfo.title;
                var tmp_authors = hasil.items[i].volumeInfo.authors;
                var tmp_imgLinks = hasil.items[i].volumeInfo.imageLinks;

                if (tmp_imgLinks == undefined){
                    var tmp_thumbnails = 'https://www.yiiframework.com/image/books/header.svg';
                }
                else{
                    var tmp_thumbnails = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                }
                obj_hasil.append('<tr> <th scope="row">'+ String((i+1)) + '<td><img src=' + tmp_thumbnails + '></td><td>'+ tmp_title +  '</td><td>' + tmp_authors+ '</tr>');
            }
        }
    });
});

$( document ).ready(function(){
    var url_utk_dipanggil = /response/;
    $.ajax({
        data:{'q': 'harry'},
        url: url_utk_dipanggil,
        success: function(hasil){
            var obj_hasil = $('#tbody');
            obj_hasil.empty();

            
            for (i = 0; i < hasil.items.length; i++) {
                var tmp_title = hasil.items[i].volumeInfo.title;
                var tmp_authors = hasil.items[i].volumeInfo.authors;
                var tmp_thumbnails = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                
                obj_hasil.append('<tr> <th scope="row">'+ String((i+1)) + '<td><img src=' + tmp_thumbnails + '></td><td>'+ tmp_title +  '</td><td>' + tmp_authors+ '</tr>');
            }
        }
    });
});