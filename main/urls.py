from django.urls import path

from main import views
from story1 import views as st1

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('education/', views.education, name='education'),
    path('experiences/', views.experiences, name='experiences'),
    path('contact/', views.contact, name='contact'),
    path('movie/', views.movie, name='movie'),
    path('listmatkul/', views.listmatkul, name='listmatkul'),
    path('matkulform/', views.matkulform, name='matkulform'),
    path('deletematkul/<str:pk_test>/', views.deletematkul, name='deletematkul'),
    path('kegiatanku/', views.fungsi_kegiatanku, name='kegiatanku'),
    path('kegiatanku/<str:id_kegiatan>/', views.fungsi_eksekusi_nama, name='kegiatanku_eksekusi'),
    path('formulirkegiatan/', views.formulir_kegiatan, name='formulir_kegiatan'),
]
