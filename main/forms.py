from django import forms
from .models import *

class MatkulForm(forms.ModelForm):
    SEMESTER = (
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Gasal 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        )

    nama_matkul = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Masukkan Nama Matkul Kamu...'}))
    nama_dosen = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Masukkan Nama Dosennya...'}))
    jumlah_sks = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    deskripsi_matkul = forms.CharField(max_length=200, widget=forms.Textarea(attrs={'class': 'form-control'}))
    semester_tahun = forms.TypedChoiceField(widget = forms.Select(attrs={'class': 'form-control', 'placeholder':'Choose...'}), choices = SEMESTER)
    ruang_kelas = forms.CharField(max_length=10,  widget=forms.TextInput(attrs={'class': 'form-control'}))
    
    class Meta:
        model = Matkul
        fields = ('nama_matkul', 'nama_dosen', 'jumlah_sks', 'deskripsi_matkul', 'semester_tahun', 'ruang_kelas')

class KegiatanForm(forms.ModelForm):
    nama_kegiatan = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Masukkan nama kegiatan'}))
    gambar = forms.URLField()

    class Meta:
        model = Kegiatan
        fields = ('nama_kegiatan', 'gambar',)

class NamaForm(forms.ModelForm):
    nama = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Masukkan nama kamu'}))

    class Meta:
        model = Person
        fields = ('nama',)
