$(document).ready(function () {

    $('.btn-secondary').click(function(){
    var current = $(this).closest('.card');
    current.prev().before(current);
  });

  $('.btn-warning').click(function(){
      var current = $(this).closest('.card');
      current.next().after(current);
  });

  $( "#accordion" )
  .accordion({
    header: "> div > h3",
    collapsible : true,
  })

});
