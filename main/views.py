from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.contrib.auth.decorators import login_required


def home(request):
    return render(request, 'main/home.html')

@login_required
def education(request):
    return render(request, 'main/education.html')

@login_required   
def experiences(request):
    return render(request, 'main/experiences.html')

@login_required
def contact(request):
    return render(request, 'main/contact.html')

@login_required
def movie(request):
    return render(request, 'main/movie.html')

@login_required
def listmatkul(request):
    matkul = Matkul.objects.all()
    return render(request, 'main/listmatkul.html', {'matkul' : matkul})


def matkulform(request):
    form = MatkulForm()
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/listmatkul')
    context = {'form':form}
    return render(request, 'main/matkulform.html', context)

def deletematkul(request, pk_test):
    namaMatkul = Matkul.objects.get(id=pk_test)
    form = MatkulForm()
    if request.method == "POST":
        namaMatkul.delete()
        return redirect('/listmatkul')
    context = {'item': namaMatkul }
    return render (request, 'main/delete.html', context)

@login_required
def fungsi_kegiatanku(request):
    kegiatan = Kegiatan.objects.all()
    form = NamaForm()
    if request.method == "POST":
        form = NamaForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('/kegiatanku')
    response = {'kegiatan' : kegiatan, 'form' : form}
    return render(request, 'main/template_kegiatanku.html', response)

@login_required
def fungsi_eksekusi_nama(request,id_kegiatan):
    kegiatannya = Kegiatan.objects.get(id=id_kegiatan)
    orang = Person.objects.all()
    form = NamaForm()
    if request.method == "POST":
        form = NamaForm(request.POST)
        if form.is_valid:
            orang = form.save()
            orang.save()
            kegiatannya.participants.add(orang)
        return redirect('/kegiatanku')
    return render(request, 'main/template_kegiatanku.html')

@login_required
def formulir_kegiatan(request):
    form = KegiatanForm()
    if request.method == "POST":
        form = KegiatanForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('/kegiatanku')
    response = {'form' : form}
    return render(request, 'main/formulir_kegiatan.html', response)