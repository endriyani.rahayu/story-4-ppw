import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from main.models import *
from main import views
import unittest
from django.contrib.auth.models import User



@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

# @tag('functional')
# class FunctionalTest(StaticLiveServerTestCase):
#     def setUp(self) -> None:
#         self.driver = webdriver.Chrome()

#     def test_formulir_kegiatan_funct(self):
#         self.driver.get(f"{self.live_server_url}/matkulform/")

#         input_nama_matkul = self.driver.find_element_by_id('id_nama_matkul')
#         input_nama_matkul.send_keys("MPK Agama Islam")

#         time.sleep(3)

#         input_nama_dosen = self.driver.find_element_by_id('id_nama_dosen')
#         input_nama_dosen.send_keys("Pak Achmad")
        
#         time.sleep(3)

#         input_jumlah_sks = self.driver.find_element_by_id('id_jumlah_sks')
#         input_jumlah_sks.send_keys(2)
        
#         time.sleep(3)

#         input_deskripsi_matkul = self.driver.find_element_by_id('id_deskripsi_matkul')
#         input_deskripsi_matkul.send_keys("Belajar Agama Islam")
        
#         time.sleep(3)

#         input_semester_tahun = self.driver.find_element_by_id('id_semester_tahun')
#         input_semester_tahun.send_keys("Gasal 2020/2021")
#         time.sleep(3)

#         input_ruang_kelas = self.driver.find_element_by_id('id_ruang_kelas')
#         input_ruang_kelas.send_keys("R 3.303")
        
#         time.sleep(3)
        
#         submit = self.driver.find_element_by_tag_name(name='button')
#         time.sleep(3)
        
#         submit.click()
#         time.sleep(3)

#         table_matkul = self.driver.find_element_by_tag_name("td")

#         self.assertIn(
#             "MPK Agama Islam",
#             [data.text for data in table_matkul]
#         )

#     def tearDown(self) -> None:
#         self.driver.quit()

class LoginTestCase(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')

    def testLogin(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')

        response = self.client.get(reverse('main:education'))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('main:experiences'))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('main:contact'))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('main:movie'))
        self.assertEqual(response.status_code, 200)

    def test_url_list_matkul(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        matkul = Matkul.objects.all()
        response = self.client.get('/listmatkul/', data={'matkul' : matkul})
        self.assertEqual(200,response.status_code)

    def test_url_kegiatanku(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/kegiatanku/')
        self.assertEqual(200,response.status_code)
    
    def test_func_page(self):
        found = resolve('/kegiatanku/')
        self.assertEqual(found.func, views.fungsi_kegiatanku)

    def test_template_kegiatanku(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/kegiatanku/')
        self.assertTemplateUsed(response, 'main/template_kegiatanku.html')

    def test_view_di_dalam_kegiatanku(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/kegiatanku/')
        isi_html_respon = response.content.decode('utf8') #masih bertipe byte string, harus di encode ke string
        self.assertIn("Let's play!", isi_html_respon)
        self.assertIn("Halo teman-teman, jika kamu tertarik untuk tahu dan mengikuti salah satu kegiatan ku, keep scrolling !", isi_html_respon)
        self.assertIn("Join the activities that you want!", isi_html_respon)

    def test_url_formulir_kegiatan(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/formulirkegiatan/')
        self.assertEqual(200,response.status_code)

    def test_template_formulir_kegiatan(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/formulirkegiatan/')
        self.assertTemplateUsed(response, 'main/formulir_kegiatan.html')

    def test_view_di_dalam_formulir_kegiatan(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/formulirkegiatan/')
        isi_html_respon = response.content.decode('utf8') #masih bertipe byte string, harus di encode ke string
        self.assertIn("Nama kegiatan", isi_html_respon)
        self.assertIn("Masukkan nama kegiatan", isi_html_respon)
        self.assertIn('<form action= " " method="POST">', isi_html_respon)
        self.assertIn('<input type="submit" name="submit"', isi_html_respon)
    
    def test_formulir_kegiatan_a_POST_request(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        self.client.post("/formulirkegiatan/", data={'nama_kegiatan': 'Fishing', 'gambar' : 'https://image.freepik.com/free-photo/horizontal-shot-pretty-dark-skinned-woman-with-afro-hairstyle-has-broad-smile-white-teeth-shows-something-nice-friend-points-upper-right-corner-stands-against-wall_273609-16442.jpg', 'participants':'Ayu'})
        jumlah = Kegiatan.objects.filter(nama_kegiatan='Fishing').count()
        self.assertEqual(jumlah, 1)
    
    def test_fungsi_kegiatan_a_POST_request(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            email='lennon@thebeatles.com', 
            password='johnpassword')
        self.client.login(username='john', password='johnpassword')
        self.client.post('/kegiatanku/', data={'nama': 'Ayu'})
        jumlah = Person.objects.filter(nama='Ayu').count()
        self.assertEqual(jumlah, 1)

    def test_deletematkul_nama_a_POST_request(self):
        obj = Matkul.objects.create(nama_matkul='Unit Test2')
        Client().post('/listmatkul/post-matkul' + str(obj.id) , data={'nama_matkul': obj })
        jumlah = Matkul.objects.filter(nama_matkul="Unit Test2").count()
        self.assertEqual(jumlah, 1)

    def test_fungsi_eksekusi_nama_a_POST_request(self):
        obj = Kegiatan.objects.create(nama_kegiatan='Unit Test2')
        Client().post('/kegiatanku/post-nama/' + str(obj.id), data={'participants': 'Ayu'})
        jumlah = Kegiatan.objects.filter(nama_kegiatan='Unit Test2').count()
        self.assertEqual(jumlah, 1)

    def test_matkulform_nama_a_POST_request(self):
        Client().post("/matkulform/", data={
                                            'nama_matkul': 'Unit Matkul',
                                            'nama_dosen': 'Ibu Fatimah',
                                            'jumlah_sks': 4,
                                            'deskripsi_matkul': 'Unit Test2',
                                            'semester_tahun': 'Gasal 2020/2021',
                                            'ruang_kelas': 'R 4.404',
                                            })
        jumlah = Matkul.objects.filter(nama_matkul='Unit Matkul').count()
        self.assertEqual(jumlah, 1)

    def test_string_representation_kegiatan(self):
        object_kegiatan = Kegiatan(nama_kegiatan="Kegiatan object")
        self.assertEqual(str(object_kegiatan), object_kegiatan.nama_kegiatan)

    def test_string_representation_person(self):
        object_person = Person(nama="Person object")
        self.assertEqual(str(object_person), object_person.nama)
    
    def test_string_representation_matkul(self):
        object_matkul = Matkul(nama_matkul="Matkul object")
        self.assertEqual(str(object_matkul), object_matkul.nama_matkul)

# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
