from django.db import models

# Create your models here.
class Matkul(models.Model):
    SEMESTER = (
            ('Gasal 2019/2020', 'Gasal 2019/2020'),
            ('Genap 2019/2020', 'Gasal 2019/2020'),
            ('Gasal 2020/2021', 'Gasal 2020/2021'),
            )
    nama_matkul = models.CharField(max_length=200)
    nama_dosen = models.CharField(max_length=200)
    jumlah_sks = models.IntegerField(default = 0)
    deskripsi_matkul = models.TextField(max_length=200)
    semester_tahun = models.CharField(max_length=200, choices = SEMESTER)
    ruang_kelas = models.CharField(max_length=10)

    def __str__(self): #untuk menampilkan nama_matkul bukan matkul_object
        return self.nama_matkul


class Person(models.Model):
    nama = models.CharField(max_length=100)
    def __str__ (self):
        return self.nama

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=100)
    gambar = models.URLField(max_length=500)
    participants = models.ManyToManyField(Person)
    def __str__(self):
        return self.nama_kegiatan
